package com.example.kropp.mini_projekt21;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button sendButton;
    private EditText message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sendButton = (Button) findViewById(R.id.sendButton);
        message = (EditText) findViewById(R.id.messageText);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("com.kropp.SEND_MESSAGE");
                intent.putExtra("msg", message.getText().toString());
                sendBroadcast(intent);

                Toast toast = Toast.makeText(getApplicationContext(), "Message sent!", Toast.LENGTH_SHORT);
                toast.show();

                message.setText("");
            }
        });
    }
}
